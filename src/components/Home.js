import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import {withStyles ,  MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBCol} from 'mdbreact';

import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Link} from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';


import {blue} from '@material-ui/core/colors';


const styles = theme => ({

    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,


    },
    heroButtons: {
        marginTop: theme.spacing.unit * 4,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            height: '100%',
            marginLeft: 'auto',
            marginRight: 'auto',

        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,



    },
    avatar: {
        margin: theme.spacing.unit,


    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,


    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    footer:

        {
            height: '10px',
            marginTop: '-50px'
        },



});
const theme = createMuiTheme({
    palette: {
        primary:
            {
                main:
                    blue[900]},
    },

});


export function HomePage(props) {
    const {classes} = props;

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="static" className={classes.appBar} >
                <AppBar position="static" >
                    <div className={classes.root}>
                        <Toolbar>
                            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                                <MenuIcon />
                            </IconButton>
                            <Typography variant="h6" color="inherit" className={classes.grow}>
                                Open Learning Analytics Platform (OpenLAP)

                            </Typography>

                            <Button color="inherit" >Logout</Button>
                        </Toolbar>
                    </div>
                </AppBar>
            </AppBar>
            <main>
                {/* Hero unit */}
                <div className={classes.heroUnit}>
                    <div className={classes.heroContent}>
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Indicator Engine Admin
                        </Typography>
                        <Typography variant="h6" align="center" color="textSecondary" paragraph>
                            Select your Goal (LA objective) , Define Question and different Indicators as per your need , This Tool contains overal GQI Approach (Goal Question & Indicator)
                        </Typography>

                    </div>
                </div>
                <div className={classNames(classes.layout, classes.cardGrid)}>
                    {/* End hero unit */}
                    <br/>
                    <Grid container spacing={40}>

                        <Grid md={4}>
                            <br/>
                            <MDBCol>
                                <MDBCard style={{width: "22rem"}}>
                                    <img src="assets/monitoring.jpg" alt="photo" style={{height: "15rem"}}/>

                                    <MDBCardBody>
                                        <MDBCardTitle>Start GQI Journey</MDBCardTitle>
                                        <MDBCardText>
                                            Goals are your LA Objective , you select different goals from given option
                                            and then Create your Question and further define your Indicators. OpenLAP
                                            Indicator Engine follows GQI Approach
                                        </MDBCardText>
                                        <Link to="/Select_Goals">
                                            <Button variant="contained" color="primary" >Select
                                                Goals</Button></Link>
                                    </MDBCardBody>
                                </MDBCard>

                            </MDBCol>
                        </Grid>



                        <Grid md={4}>
                            <br/>

                            <MDBCol>
                                <MDBCard style={{width: "22rem"}}>
                                    <img src="assets/recomm.jpg" style={{height: "15rem"}} alt="photo"/>

                                    <MDBCardBody>
                                        <MDBCardTitle>View Existing Indicators</MDBCardTitle>
                                        <MDBCardText>
                                            You want to see and check how the existing indicators lookslike , here you
                                            can see all the existing indicators which are stored in our system.
                                        </MDBCardText>

                                        <Link to ="/View_Eindicators">
                                            <Button variant="contained" color="primary" href="#">Existing Indicators</Button>

                                        </Link>

                                    </MDBCardBody>


                                </MDBCard>
                            </MDBCol>

                        </Grid>



                        <Grid md={4}>
                            <br/>

                            <MDBCol>
                                <MDBCard style={{width: "22rem"}}>
                                    <img src="assets/help.jpg" style={{height: "15rem"}} alt="photo"/>

                                    <MDBCardBody>
                                        <MDBCardTitle>FAQ</MDBCardTitle>
                                        <MDBCardText>
                                            Commonly Asked Questions regarding , what is Goal ? why to select which Goal
                                            and other information about the working flow of the system.
                                        </MDBCardText>
                                        <Link to ="/FAQ"><Button variant="contained" color="primary" href="#">Frequently Asked Question</Button></Link>
                                    </MDBCardBody>


                                </MDBCard>
                            </MDBCol>

                        </Grid>

                    </Grid>










                </div>
                <br/><br/><br/>
                <div className={classes.footer}>
                    <br/><br/><br/>
                    <footer class="page-footer text-center font-small mdb-color darken-2 "  >

                        <hr/>

                        <div className="pb-4" style={{height: "4rem" }}>

                            <h6 style={{}}>Follow us on Git Hub </h6>
                            <a href="https://github.com/mdbootstrap/bootstrap-material-design" target="_blank">
                                <i class="fab fa-github mr-3"></i>
                            </a>


                        </div>


                    </footer>
                </div>

            </main>

            {/*/!* Footer *!/*/}

            {/* End footer */}

        </React.Fragment>
    );
}

HomePage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HomePage);

