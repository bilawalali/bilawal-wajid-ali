import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {withStyles ,  MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBCol} from 'mdbreact';
import {Bootstrap} from 'react-bootstrap';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Link} from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {blue} from '@material-ui/core/colors';

const styles = theme => ({

    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    heroButtons: {
        marginTop: theme.spacing.unit * 4,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            height: 'auto',
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },

    footer:

        {
            height: '10px',
            marginTop: '-50px'
        }


});

const theme = createMuiTheme({
    palette: {
        primary:
            {
                main:
                    blue[900]},
    },

});

export function HomePage(props) {
    const {classes} = props;

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="static" className={classes.appBar} >
            <AppBar position="static" >
                <div className={classes.root}>
                <Toolbar>
                    <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" color="inherit" className={classes.grow}>
                        Open Learning Analytics Platform (OpenLAP)

                    </Typography>

                    <Button color="inherit" >Logout</Button>
                </Toolbar>
                </div>
            </AppBar>
            </AppBar>
            <main>
                {/* Hero unit */}
                <div className={classes.heroUnit}>
                    <div className={classes.heroContent}>
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Indicator Engine Admin
                        </Typography>
                        <Typography variant="h6" align="center" color="textSecondary" paragraph>
                            This is the Admin Panel
<br/>
                            Admin Can upload Visualization and Analytics Module and moderate the different pages as per need
                        </Typography>

                    </div>
                </div>
                <div className={classNames(classes.layout, classes.cardGrid)}>
                    {/* End hero unit */}
                    <br/>
                    <Grid container spacing={40}>


                        <Grid md={4}>
                            <br/>
                            <MDBCol>
                                <MDBCard style={{width: "22rem"}}>
                                    <img src="assets/monitoring.jpg" alt="photo" style={{height: "12rem"}}/>

                                    <MDBCardBody>
                                        <MDBCardTitle>Start GQI Journey</MDBCardTitle>
                                        <MDBCardText>
                                            Goals are your LA Objective , you select different goals from given option
                                            and then Create your Question and further define your Indicators. OpenLAP
                                            Indicator Engine follows GQI Approach
                                        </MDBCardText>
                                        <Link to="/Select_Goals">
                                            <Button variant="contained" color="primary">Start Journey
                                                </Button></Link>
                                    </MDBCardBody>
                                </MDBCard>

                            </MDBCol>
                        </Grid>


                        <Grid md={4}>
                            <br/>
                            <MDBCol>
                                <MDBCard style={{width: "22rem"}}>
                                    <img src="assets/predition.jpg" style={{height: "10.5rem"}} alt="photo"/>

                                    <MDBCardBody>
                                        <MDBCardTitle>Upload Analytics Methods</MDBCardTitle>
                                        <MDBCardText>
                                            Just Like goals, OpenLAP Indicator Engine enables you to upload different
                                            Analytics Method , so here you have that option to upload different
                                            Analytics Method as per your need.
                                        </MDBCardText>
                                        <br/>
                                        <Link to ="/Upload_Amodules"> <Button variant="contained" color="primary">Upload Analytics Methods</Button></Link>
                                    </MDBCardBody>


                                </MDBCard>
                            </MDBCol>


                        </Grid>

                        <Grid md={4}>
                            <br/>
                            <MDBCol>
                                <MDBCard style={{width: "22rem"}}>
                                    <img src="assets/reflect.jpg" style={{height: "8.7rem"}} alt="photo"/>

                                    <MDBCardBody>
                                        <MDBCardTitle>Upload Visualization Modules</MDBCardTitle>
                                        <MDBCardText>
                                            OpenLAP Indicator Engine also enables you to upload different Visualization
                                            Modules , so here you have that option to upload different visualization
                                            modules as per your need.
                                        </MDBCardText>
                                        <br/>
                                        <Link to ="/Upload_Vmodules">  <Button variant="contained" color="primary">Upload Visualization</Button> </Link>
                                    </MDBCardBody>


                                </MDBCard>
                            </MDBCol>


                        </Grid>

                        <Grid md={4}>
                            <br/>
                            <MDBCol>
                                <MDBCard style={{width: "22rem"}}>
                                    <img src="assets/recomm.jpg" style={{height: "10rem"}} alt="photo"/>

                                    <MDBCardBody>
                                        <MDBCardTitle>View Existing Indicators</MDBCardTitle>
                                        <MDBCardText>
                                            You want to see and check how the existing indicators lookslike , here you
                                            can see all the existing indicators which are stored in our system.
                                        </MDBCardText>
                                        <br/>
                                        <Link to ="/View_Eindicators"> <Button variant="contained" color="primary" href="#">Existing Indicators</Button> </Link>
                                    </MDBCardBody>


                                </MDBCard>
                            </MDBCol>


                        </Grid>

                        <Grid md={4}>
                            <br/>

                            <MDBCol>
                                <MDBCard style={{width: "22rem"}}>
                                    <img src="assets/help.jpg" style={{height: "11.5rem"}} alt="photo"/>

                                    <MDBCardBody>
                                        <MDBCardTitle color="inherit">FAQ</MDBCardTitle>
                                        <MDBCardText>
                                            Commonly Asked Questions regarding , what is Goal ? why to select which Goal
                                            and other information about the working flow of the system.
                                        </MDBCardText>
                                        <Link to ="/FAQ"><Button variant="contained" color="primary" href="#">Frequently Asked Questions</Button></Link>
                                    </MDBCardBody>


                                </MDBCard>
                            </MDBCol>

                        </Grid>



                    </Grid>


                </div>
<br/><br/><br/>
            </main>

            {/*/!* Footer *!/*/}
            <div className={classes.footer}>
                <br/><br/><br/>
                <footer class="page-footer text-center font-small mdb-color darken-2 "  >

                    <hr/>

                    <div className="pb-4" style={{height: "4rem" }}>

                        <h6 style={{}}>Follow us on Git Hub </h6>
                        <a href="https://github.com/mdbootstrap/bootstrap-material-design" target="_blank">
                            <i class="fab fa-github mr-3"></i>
                        </a>


                    </div>


                </footer>
            </div>
            {/* End footer */}
        </React.Fragment>
    );
}

HomePage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HomePage);

