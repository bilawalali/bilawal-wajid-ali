import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import {withStyles} from '@material-ui/core/styles';
import {MDBSelect, MDBSelectInput, MDBSelectOptions, MDBSelectOption} from "mdbreact";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import GoalQuestion from './Select_Goals/GoalQuestion';
import IndicatorPanel from './Select_Goals/IndicatorPanel';

const styles = theme => ({

    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    heroButtons: {
        marginTop: theme.spacing.unit * 4,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    footer:

        {
            height: '10px',
            marginTop: '-50px'
        }

});


class NavTabs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            goal: '',
            question: '',
            organization: '',
            store: '',
            platform: '',
            activityProvider: '',
            activityProviderSource: '',
            verb: '',
            value: '',
        };
    };

    render() {
        const {classes} = this.props;
        const {value} = this.state;

        return (
            <React.Fragment>

                <CssBaseline />
                <AppBar position="static" className={classes.appBar}>
                    <AppBar position="static">
                        <div className={classes.root}>
                            <Toolbar>
                                <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                                    <MenuIcon />
                                </IconButton>
                                <Typography variant="h6" color="inherit" className={classes.grow}>
                                    Open Learning Analytics Platform (OpenLAP)

                                </Typography>

                                <Button color="inherit">Logout</Button>


                            </Toolbar>
                        </div>
                    </AppBar>
                </AppBar>

                <div className={classes.heroUnit}>
                    <div className={classes.heroContent}>
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Indicator Engine
                        </Typography>
                        <Typography variant="h6" align="center" color="textSecondary" paragraph>
                            Select Goal your LA objective , Create Question and then define indicators for the end
                            result
                        </Typography>

                    </div>
                </div>

                <div className={classes.layout}>

                    <Grid>

                        < GoalQuestion/>

                    </Grid>
                    <br/>
                    <Grid>

                        < IndicatorPanel/>

                    </Grid>
                    <div><br/></div>

                </div>
                <div className={classes.footer}>
                    <br/><br/><br/>
                    <footer class="page-footer text-center font-small mdb-color darken-2 "  >

                        <hr/>

                        <div className="pb-4" style={{height: "4rem" }}>

                            <h6 style={{}}>Follow us on Git Hub </h6>
                            <a href="https://github.com/mdbootstrap/bootstrap-material-design" target="_blank">
                                <i class="fab fa-github mr-3"></i>
                            </a>


                        </div>


                    </footer>
                </div>
            </React.Fragment>
        );
    }

}
NavTabs.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavTabs);