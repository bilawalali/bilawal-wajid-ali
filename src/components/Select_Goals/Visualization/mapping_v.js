/**
 * Created by User on 25.04.2019.
 */
import React from 'react';
import {Button} from '@material-ui/core';
import Table_v from './Table_v';
import PropTypes from 'prop-types';
import {MDBBtn} from 'mdbreact';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import VLibrary from './VLibrary';
import Vc_type from './Vc_type';
import {blue} from '@material-ui/core/colors';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import PositionedSnackbar from '../Mapping/PositionedSnackbar';

const styles = theme => ({
    root: {
        position: 'relative',

        height: 200,

        width: 400,


        backgroundColor: theme.palette.background.paper,
    },
    formControl: {
        margin: theme.spacing.unit,

        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    control: {

        position: 'absolute',
        left: 'auto',
        width: 300,
        height: 120,


    },
    controlf: {

        position: 'absolute',
        left: 520,
        width: 300,
        height: 120,
        bottom: 45,


    },

});

const theme = createMuiTheme({
    palette: {
        primary:
            {
                main:
                    blue[900]},
    },

});
class Mappingv extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstColumn1: [],
            secondColumn1: [],
            snackbar: false
        };
    }

    addItem = () => {
        var colOne = document.getElementById("userID22");
        var strUser = colOne.options[colOne.selectedIndex].value;

        var colTwo = document.getElementById("userID23");
        var strUser2 = colTwo.options[colTwo.selectedIndex].value;

        if(isNaN(strUser) && isNaN(strUser2)){
        }else if(!isNaN(strUser) && !isNaN(strUser2)){

        }
        else {
            this.setState({
                snackBar: true
            });
            return ;
        }

        this.setState({
            firstColumn1: [...this.state.firstColumn1, strUser],
            secondColumn1: [...this.state.secondColumn1, strUser2]
        });
    };

    render() {
        const {classes} = this.props;
        return (
            <Grid>
                <br/>
                <div>
                    <h5><strong><label class="bmd-label-floating" style={{marginLeft: "1.5%"}}>Visualization:
                    </label></strong></h5>
                    <br/>
                    <div className={classes.formControl}><VLibrary/></div>
                    <br/>
                    <div>
                        <Vc_type/>
                    </div>
                    <br/>
                    <br/>
                    <div>


                        <h7><strong><label class="bmd-label-floating" style={{marginLeft: "1.5%"}}>Same attributes can be map for
                            i.e Text to Text and Numeric to Numeric:
                        </label></strong></h7>
                        <br/>
                        <br/>
                    </div>
                    <br/>
                    <br/>
                </div>
                <div className={classes.root}>


                    <div>
                        <h6><strong><label class="bmd-label-floating" style={{marginLeft: "10%"}}>Output of
                            Method(s):</label></strong></h6>


                        <select name="user" id="userID22" size="3" className={classes.control}
                                style={{marginLeft: "10%"}}>
                            <option value={'John'}>CourseID (Text)</option>
                            <option value={123}>TimeStamp (Numeric)</option>
                            <option value={'paul'}>Course Name(Text)</option>

                        </select>


                        <div>

                            <h6><strong><label class="bmd-label-floating"
                                               style={{left: '130%', bottom: 170, position: 'absolute', width: '45%'}}>Visualization
                                Methods: </label></strong></h6>
                        </div>

                        <select name="user" id="userID23" size="3" className={classes.controlf}>
                            <option value={'CS-1255'}>CourseID (Text)</option>
                            <option value={123}>TimeStamp (Numeric)</option>
                            <option value={'Course Name'}>Course Name(Text)</option>
                        </select>
                        <Button variant="contained" color="primary" onClick={this.addItem}
                                style={{float: 'right', left: '25%', width: '30%', top: 30}}>Map</Button>


                    </div>


                </div>
                <div>
                    {this.state.firstColumn1.length > 0 ?
                        <Table_v col1={this.state.firstColumn1} col2={this.state.secondColumn1}/> : ''
                    }
                </div>
                <Button variant="contained" color="primary" onClick={this.handleClickOpen} style={{marginLeft: "4%"}}>
                    Visualize
                </Button>

                <hr/>
                <Button variant="contained" color="primary" onClick={this.handleClickOpen} style={{marginLeft: "80%"}}>
                    Associate
                </Button>



                {this.state.snackBar ? <PositionedSnackbar/> : ''}
            </Grid>

        );
    }
}
Mappingv.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Mappingv);