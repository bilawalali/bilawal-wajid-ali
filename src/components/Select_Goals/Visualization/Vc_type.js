/**
 * Created by User on 25.04.2019.
 */
/**
 * Created by User on 25.04.2019.
 */
/**
 * Created by User on 12.04.2019.
 */


import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '77%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit/4,
    },
    noLabel: {
        marginTop: theme.spacing.unit * 3,
    },

});








class Example extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            vc_me:'',



        };
    };
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };



    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>

                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel htmlFor="age-simple"></InputLabel>
                    <div>
                        <h6><strong><label class="bmd-label-floating" style={{ marginLeft: "2%"}}>Select Type of Visualization:</label></strong></h6>
                    </div>
                    <Select
                        value={this.state.vc_me}
                        onChange={(event) => this.handleChange(event)}
                        style={{ marginLeft: "1%"}}
                        inputProps={{
                            name: 'vc_me',


                        }}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value="Bar Chart">Bar Chart</MenuItem>
                        <MenuItem value="Pie Chart">Pie Chart</MenuItem>
                        <MenuItem value="Line Chart">Line Chart</MenuItem>

                    </Select>
                </FormControl>








            </div>
        );
    }
}

Example.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Example);
