/**
 * Created by User on 25.04.2019.
 */
/**
 * Created by User on 12.04.2019.
 */


import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '77%',
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit/4,
    },
    noLabel: {
        marginTop: theme.spacing.unit * 3,
    },

});








class Example extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            libar:'',



        };
    };
    handleChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };



    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>

                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel htmlFor="age-simple"></InputLabel>
                    <div>
                        <h6><strong><label class="bmd-label-floating" style={{ marginLeft: "1%"}}>Select Visualization Library:</label></strong></h6>
                    </div>
                    <Select
                        value={this.state.libar}
                        onChange={(event) => this.handleChange(event)}
                        style={{ marginLeft: "1%"}}
                        inputProps={{
                            name: 'libar',


                        }}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value="C3/D3">C3/D3</MenuItem>
                        <MenuItem value="GoogleCharts">GoogleCharts</MenuItem>
                        <MenuItem value="Source Path">Source Path</MenuItem>

                    </Select>
                </FormControl>








            </div>
        );
    }
}

Example.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Example);
