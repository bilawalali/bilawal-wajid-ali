import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },

    container :{

        minWidth:800,
    },

    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
});



class Radio_Option_User extends React.Component {
    state = {
        expanded: null,
    };

    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    render() {
        const { classes } = this.props;
        const { expanded } = this.state;

        return (
            <div className={classes.root}>
                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Select Date</Typography>
                        <Typography className={classes.secondaryHeading}>Select the valid Date from the given option</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            <form className={classes.container} noValidate>
                            <TextField
                                id="date"
                                label="From Date"
                                type="date"
                                defaultValue=""
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />



                        </form>
<br/> <br/>
                            <form className={classes.container} noValidate>



                                <TextField
                                    id="date"
                                    label="Till Date"
                                    type="date"
                                    defaultValue=""
                                    className={classes.textField}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </form>
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChange('panel2')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Users</Typography>
                        <Typography className={classes.secondaryHeading}>
                            Select to which extends your data can be use
                        </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>


                                <FormControl component="fieldset" className={classes.container}>
                                    <FormLabel component="legend">Data Filteration</FormLabel>
                                    <RadioGroup
                                        aria-label="gender"
                                        name="gender2"
                                        className={classes.group}
                                        value={this.state.value}

                                        onChange={this.handleChange}
                                    >
                                        <FormControlLabel
                                            value="Use all my data"
                                            control={<Radio color="primary" />}
                                            label="Use all my data"
                                            labelPlacement="left"

                                        />
                                        <FormControlLabel
                                            value="Use only my data"
                                            control={<Radio color="primary" />}
                                            label="Use only my data"
                                            labelPlacement="left"
                                        />
                                        <FormControlLabel
                                            value="Exclude my data"
                                            control={<Radio color="primary" />}
                                            label="Exclude my data"
                                            labelPlacement="left"
                                        />

                                    </RadioGroup>

                                </FormControl>

                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>


            </div>
        );
    }
}

Radio_Option_User.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Radio_Option_User);
