import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import NoSsr from '@material-ui/core/NoSsr';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Multi_Select_Filters from './Multi_Select_Filter';
import Radio_Option_User from './Radio_Option_User';
import Additional_Att from './Additional_Att';

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

function LinkTab(props) {
    return <Tab component="a" onClick={event => event.preventDefault()} {...props} />;
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        width:'88%',
    },
    formControl: {
        margin: theme.spacing.unit,

        width:'90%',
    },

});

class NavTabs extends React.Component {
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        return (
            <NoSsr>
            <br/>
                <div>
                    <h5><strong><label class="bmd-label-floating" style={{ marginLeft: "1.5%"}}>Filter(s):
                    </label></strong></h5>
                    <br/>
                </div>

                <div className={classes.root} >
                    <AppBar position="static"  style={{ marginLeft: "2%"}} >
                        <Tabs variant="fullWidth" value={value} onChange={this.handleChange}  >
                            <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="Activity Attributes"/>
                            <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="User/Time Stamp"/>

                        </Tabs>
                    </AppBar>
                    {value === 0 && <TabContainer>


                        <div className={classes.formControl}><Multi_Select_Filters/></div>
                        <div><br/></div>
                        <div className={classes.formControl}><Additional_Att/></div>

                    </TabContainer>}
                    {value === 1 && <TabContainer>
                        
                        <Radio_Option_User classes=""/>
                    </TabContainer>}

                </div>
            </NoSsr>
        );
    }
}

NavTabs.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavTabs);
