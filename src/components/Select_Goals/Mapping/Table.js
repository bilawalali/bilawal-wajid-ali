import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
const styles = theme => ({
    root: {
        width: '76%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        marginLeft:'3%',
    },
    table: {
        minWidth: 400,
    },
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
}


function SimpleTable(props) {
    const { classes } = props;
    var rows = [];

    props.col1.forEach((items, index) => {
        rows.push(createData(props.col1[index], props.col2[index]));
    });

    return (
    <Grid>
        <div>
            <h5><strong><label class="bmd-label-floating" style={{ marginLeft: "1.5%"}}>Mapped Values:
            </label></strong></h5>

        </div>
        <Paper className={classes.root}>

            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Data Columns</TableCell>
                        <TableCell align="right">Input Methods</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row">
                                {row.name}
                            </TableCell>
                            <TableCell align="right">{row.calories}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    </Grid>
    );
}

SimpleTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTable);