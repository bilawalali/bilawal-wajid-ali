import React from 'react';
import {Button} from '@material-ui/core';
import Table from './Table';
import PropTypes from 'prop-types';


import Grid from '@material-ui/core/Grid';
import {blue} from '@material-ui/core/colors';
import PositionedSnackbar from './PositionedSnackbar';
import {withStyles ,  MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
const styles = theme => ({
    root: {
        position: 'relative',

        height: 200,

        width: 400,


        backgroundColor: theme.palette.background.paper,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    control: {

        position: 'absolute',
        left: 'auto',
        width: 300,
        height: 120,


    },
    controlf: {

        position: 'absolute',
        left: 520,
        width: 300,
        height: 120,
        bottom: 45,


    },

});

const theme = createMuiTheme({
    palette: {
        primary:
            {
                main:
                    blue[900]},
    },

});
class Mapping extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstColumn: [],
            secondColumn: [],
            typesCol1: '',
            typesCol2: '',
            snackBar: false
        };
    }

    addItem = () => {

        var colOne = document.getElementById("userID1");
        var strUser = colOne.options[colOne.selectedIndex].value;

        var colTwo = document.getElementById("userID2");
        var strUser2 = colTwo.options[colTwo.selectedIndex].value;

        if(isNaN(strUser) && isNaN(strUser2)){
        }else if(!isNaN(strUser) && !isNaN(strUser2)){

        }
        else {
            this.setState({
                snackBar: true
            });
            return ;
        }

        this.setState({
            firstColumn: [...this.state.firstColumn, strUser],
            secondColumn: [...this.state.secondColumn, strUser2],
            snackBar: false
        });
    };

    render() {
        const {classes} = this.props;
        return (
            <Grid>

                <div>


                    <h7><strong><label class="bmd-label-floating" style={{marginLeft: "1.5%"}}>Same attributes can be map for
                        i.e Text to Text and Numeric to Numeric:
                    </label></strong></h7>
                    <br/>
                    <br/>
                </div>
                <div className={classes.root}>


                    <div>
                        <h6><strong><label class="bmd-label-floating" style={{marginLeft: "10%"}}>Data
                            Column:</label></strong></h6>


                        <select name="user" id="userID1" size="3" className={classes.control}
                                style={{marginLeft: "10%"}}>
                            <option value={'Assignment Name'}>Assignment Name(Text)</option>
                            <option value={'Assignment Owner'}>Assignment Owner(Text)</option>
                            <option value={'Assignment Submitter'}>Assignment Submitter(Text)</option>
                            <option value={123}>Assignment DueDate(Numeric)</option>
                        </select>
                        <div>

                            <h6><strong><label class="bmd-label-floating"
                                               style={{left: '130%', bottom: 170, position: 'absolute', width: '30%'}}>Input
                                Methods: </label></strong></h6>
                        </div>

                        <select name="user" id="userID2" size="3" className={classes.controlf}>
                            <option value={'Assignment Name'}>Assignment Name(Text)</option>
                            <option value={'Assignment Owner'}>Assignment Owner(Text)</option>
                            <option value={'Assignment Submitter'}>Assignment Submitter(Text)</option>
                            <option value={123}>Assignment DueDate(Numeric)</option>
                        </select>
                        <Button variant="contained" color="primary" onClick={this.addItem}
                                style={{float: 'right', left: '25%', width: '30%', top: 30}}>Map</Button>
                    </div>


                </div>
                <div>
                    {this.state.firstColumn.length > 0 ?
                        <Table col1={this.state.firstColumn} col2={this.state.secondColumn}/> : ''
                    }
                </div>
                {this.state.snackBar ? <PositionedSnackbar/> : ''}
            </Grid>
        );
    }
}
Mapping.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Mapping);