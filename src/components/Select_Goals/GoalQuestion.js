import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";
import AddnewGoal from './AddNewGoal';

const styles = theme => ({

    root: {

        paddingTop: theme.spacing.unit * 0,
        paddingBottom: theme.spacing.unit * 2,

        width:'100%',
        flexGrow: 1,
    },


    formControl: {
        margin: theme.spacing.unit,

        width:'90%',
    },

});




class PaperSheet extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            goal: '',
            question: '',
            organization: '',
            store: '',
            platform: '',
            activityProvider: '',
            activityProviderSource: '',
            verb: '',
            value: '',


        };
    };

    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});

    };

    render () {

        const { classes } = this.props;

        return (



            <div>
                <Paper className={classes.root} elevation={1}>

                    <AppBar position="static" className={classes.appBar}>
                        <Toolbar >
                            <Typography variant="h6" component="h6" color="inherit" noWrap>
                                Goal Selection ( Goal is your LA Objective)
                            </Typography>
                        </Toolbar>
                    </AppBar>

                    <br/>


                    <Typography>



                        <FormControl variant="filled" className={classes.formControl}>
                            <InputLabel htmlFor="age-simple"></InputLabel>
                            <div>
                                <h6><strong><label class="bmd-label-floating" style={{ marginLeft: "1%"}}>Goal Selection:</label></strong></h6>


                            </div>
                            <Select
                                value={this.state.goal}
                                onChange={(event) => this.handleChange(event)}
                                style={{ marginLeft: "1%"}}
                                inputProps={{
                                    name: 'goal',
                                    id: 'outlined-goal-simple',

                                }}
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value="reflection">Reflection</MenuItem>
                                <MenuItem value="recommendation">Recommendation</MenuItem>
                                <MenuItem value="monitoring">Monitoring</MenuItem>
                                <MenuItem value="feedback">Feedback</MenuItem>
                                <MenuItem value="adaption">Adaption</MenuItem>
                            </Select>

<div>
                                <AddnewGoal  />
</div>

                        </FormControl>
<br/><br/>




                        <br/>

                    </Typography>

                </Paper>

            </div>

        );
    }

}







PaperSheet.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PaperSheet);