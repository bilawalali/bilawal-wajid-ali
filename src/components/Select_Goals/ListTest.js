/**
 * Created by User on 13.04.2019.
 */
/**
 * Created by User on 11.04.2019.
 */
import React from 'react';
import DualListBox from 'react-dual-listbox';
import 'react-dual-listbox/lib/react-dual-listbox.css';
import PropTypes from 'prop-types';

const options = [
    {
        label: 'Analytics Input',
        options: [
            { value: 'luna', label: 'Moon' },
            { value: 'io', label: 'Io' },
            { value: 'europa', label: 'Europa' },
        ],
    },
    {
        label: 'Attributes',
        options: [
            { value: 'phobos', label: 'Phobos' },
            { value: 'deimos', label: 'Deimos' },
            { value: 'ganymede', label: 'Ganymede' },
            { value: 'callisto', label: 'Callisto' },
        ],
    },
    {

    },
];


class ListTest extends React.Component
{
    constructor() {
        super();

        this.state = {
            selected: ['one'],

        };
    }

    render() {
        return (

            <div>
                <div>
                    <h6><strong><label class="bmd-label-floating">Mapping Values</label></strong>
                    </h6>
                </div>
                <DualListBox
                    options={options}
                    selected={this.state.selected}
                    onChange={(selected) => {
                        this.setState({ selected });
                    }}
                />


            </div>
        );
    }
}

ListTest.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default  (ListTest);