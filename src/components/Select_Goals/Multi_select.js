import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',

    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',

    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit / 4,
    },
    noLabel: {
        marginTop: theme.spacing.unit * 3,
    },

});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,

        },
    },
};



const names = [
    'Create',
    'Update',
    'View',
    'Delete',
    'Submit',
    'Review',
    'Alter',
    'Download',
    'Save',
    'Copy',
];

function getStyles(name, that) {
    return {
        fontWeight:
            that.state.name.indexOf(name) === -1
                ? that.props.theme.typography.fontWeightRegular
                : that.props.theme.typography.fontWeightMedium,
    };
}

class MultipleSelect extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

            name: [],



        };
    };
    handleChange = event => {
        this.setState({ name: event.target.value });
    };

    handleChangeMultiple = event => {
        const { options } = event.target;
        const value = [];
        for (let i = 0, l = options.length; i < l; i += 1) {
            if (options[i].selected) {
                value.push(options[i].value);
            }
        }

    };

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>

                <div>
                    <h5><strong><label class="bmd-label-floating" >Data Set(s):
                    </label></strong></h5>
                    <br/>
                </div>

                <FormControl variant="outlined" className={classes.formControl}>
                    <div>
                        <h6><strong><label class="bmd-label-floating"> Verb(s):
                          </label></strong></h6>
                    </div>

                    <Select
                        multiple
                        value={this.state.name}
                        onChange={(event) => this.handleChange(event)}
                        input={<Input id="select-multiple-checkbox" />}
                        renderValue={selected => selected.join(', ')}
                        MenuProps={MenuProps}


                    >

                        {names.map(name => (
                            <MenuItem key={name} value={name}>
                                <Checkbox checked={this.state.name.indexOf(name) > -1} />
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>

                <div><br/></div>





            </div>
        );
    }
}

MultipleSelect.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(MultipleSelect);
