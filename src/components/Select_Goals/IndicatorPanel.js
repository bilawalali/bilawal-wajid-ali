/**
 * Created by User on 19.04.2019.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles , createMuiTheme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { dripForm } from 'react-drip-form';
import FormControl from '@material-ui/core/FormControl';
import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";
import Filters from './Filters'
import TextField from '@material-ui/core/TextField';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Multi_Select from "./Multi_select";
import MultiSelect_Platform from "./MultiSelect_Platform";
import MappingV from "./Visualization/mapping_v";
import Mapping from "./Mapping/mapping";
import LoadIndicator from '../LoadIndicator';
import Multi_select1 from './multi_select1';
import Analytics_Method from './analytics_method';
import Button from '@material-ui/core/Button';
import {blue} from '@material-ui/core/colors';
import {
    Input,
    FieldGroup,
    Checkbox,
    Radio,
} from 'react-drip-form-components';
const styles = theme => ({

    root: {

        paddingTop: theme.spacing.unit * 0,
        paddingBottom: theme.spacing.unit * 2,
        width:'100%',
        flexGrow: 1,
    },

    root_tab: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },

    formControl: {
        margin: theme.spacing.unit,

        width:'90%',
    },

});



const theme = createMuiTheme({
    palette: {
        primary:
            {
                main:
                    blue[900]},
    },

});


function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};


function LinkTab(props) {
    return <Tab component="a" onClick={event => event.preventDefault()} {...props} />;
}

class PaperSheet extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            goal: '',
            question: '',
            organization: '',
            store: '',
            platform: '',
            activityProvider: '',
            activityProviderSource: '',
            verb: '',
            value: '',



        };
    };

    componentDidUpdate = (prevState) => {
        this.scroll()
    }

    handleChange1 = (event, value) => {
        console.log("test");
        console.log(window);
        this.setState({value});
    };

    scroll = () => {
        console.log("bilawaltest");
        window.scroll(0,0);
        window.scroll(0,800);
    }

    cancelCourse = () => {
        document.getElementById("create-course-form").reset();
    }
    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});

    };
    render () {

        const { classes } = this.props;
        const {value} = this.state;
        return (



            <div>
                <Paper className={classes.root} elevation={1}>

                    <AppBar position="static" className={classes.appBar}>
                        <Toolbar >
                            <Typography variant="h6" component="h6" color="inherit" noWrap>
                                Analytic(s) and Indicator(s)  Panel
                            </Typography>
                        </Toolbar>
                    </AppBar>

                    <br/>
                    <Typography>
                        <form id="create-course-form">
                        <FormControl variant="outlined" className={classes.formControl}>

                            <div>
                                <h6><strong><label class="bmd-label-floating" style={{ marginLeft: "1%"}}>Abstract Analytics Idea:</label></strong></h6>
                            </div>
                            <TextField



                                ref="user_question"
                                variant="outlined"
                                placeholder="Enter Analytics Question"
                                style={{ marginLeft: "1%"}}
                                InputLabelProps={{

                                }}
                            />


                        </FormControl>
                            <br/> <br/>
                            <div>
                                <h7><strong><label class="bmd-label-floating" style={{ marginLeft: "2%"}}>No Associate Indicator Attached:</label></strong></h7>
                            </div>
                        <br/>
                        <Button color="primary" variant="contained" style={{ marginLeft:"2.5%"}}  onClick={this.cancelCourse}> New Question</Button>

                        </form>
                    </Typography>
                    <br/> <br/>
                    <div>
                        <h6><strong><label class="bmd-label-floating" style={{ marginLeft: "1.5%"}}>Select Indicator(s):</label></strong></h6>
                    </div>
                    <div className="{classes.root_tab}">
                        <AppBar position={'static'} color="primary">
                            <Tabs  value={value} onChange={this.handleChange1}

                                   variant="scrollable"
                                   scrollButtons="auto">
                                <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="Basic Indicator"/>
                                <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="Load Existing Indicator"/>
                                <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="Composite Indicator"/>
                                <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="MLA Indicator"/>
                            </Tabs>
                        </AppBar>
                        {value === 0 && <TabContainer>


                            <div className={classes.formControl} >
                            <Multi_Select/>


                            </div>

                            <div className={classes.formControl} >
                                <MultiSelect_Platform/>


                            </div>
                            <div><br/></div>
                            <div className={classes.formControl} >
                                <Multi_select1/>


                            </div>

                            <div><br/></div>

                            <div>
                                <Filters/>


                            </div>
                            <div><br/></div>

                            <div><br/></div>

                            <hr color="blue"/>
                            <div><br/></div>
                        <div>
                            <h5><strong><label class="bmd-label-floating" style={{marginLeft: "1.5%"}}>Analysis:
                            </label></strong></h5>
                            <br/>
                        </div>
                            <div className={classes.formControl} >
                                <Analytics_Method />


                            </div>
                            <div><br/></div>

                            <div><br/></div>
                            <div>
                                <Mapping/>
                            </div>
                            <div><br/></div>

                            <div><br/></div>


                            <hr color="blue"/>

                            <div>
                                <MappingV/>
                            </div>

                            <div><br/></div>

                        </TabContainer>}
                        {value === 1 && <TabContainer>

                            <div><LoadIndicator/></div>

                        </TabContainer>}
                        {value === 2 && <TabContainer>Page Three</TabContainer>}
                    </div>

                </Paper>

            </div>

        );
    }

}







PaperSheet.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PaperSheet);