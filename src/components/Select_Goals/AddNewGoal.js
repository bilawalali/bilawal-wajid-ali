/**
 * Created by User on 19.04.2019.
 */
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
    },
    fab: {
        // margin: theme.spacing.unit*1,
        // marginLeft:theme.spacing.unit*127,

        position: 'absolute',
        bottom: theme.spacing.unit * 0,
        top: theme.spacing.unit * 4,
        right: theme.spacing.unit * 3,
        left: theme.spacing.unit * 127,
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
    formControl: {
        margin: theme.spacing.unit,

        width:'100%',
    },



});

class FormDialog extends React.Component {
    state = {
        open: false,
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes } = this.props;
        return (

            <div>

                <Tooltip title="Request New Goal"  aria-label="Add">
                <Fab color="primary" aria-label="Add" className={classes.fab} onClick={this.handleClickOpen}>
                    <AddIcon />
                </Fab>
                </Tooltip>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Request New Goal</DialogTitle>
                    <DialogContent>
                        <DialogContentText  style={{ marginLeft: "1%"}}>
                          You can Request new Goal if you did not find any suitable goal in existing system.

                        </DialogContentText>
                        <br/>
                        <TextField

                            id="standard-full-width"

                            fullWidth={"100%"}
                            variant="outlined"
                            placeholder="For e.g Recommendation , Filtering"
                            style={{ marginLeft: "1%"}}
                            InputLabelProps={{

                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary">
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
export default withStyles(styles)(FormDialog)
