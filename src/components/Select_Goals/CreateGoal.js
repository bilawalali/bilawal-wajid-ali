import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import {MDBBtn} from 'mdbreact';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(12),
        color: theme.palette.text.secondary,
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,

    },

    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        Width:200,
    },
});
function PaperComponent(props) {
    return (
        <Draggable>
            <Paper {...props} />
        </Draggable>
    );
}

class ControlledExpansionPanels extends React.Component {
    state = {
        expanded: null,
        open: false,
    };


    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };


    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    render() {
        const { classes } = this.props;
        const { expanded } = this.state;

        return (
            <div className={classes.root}>
                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Request New Goal</Typography>
                        <Typography className={classes.secondaryHeading}><strong>Existing Goals doesn't match your needs, you can add new GOAL also.</strong></Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            <form>

                                <div><br/></div>
                                <div class="form-group">


                                    {/*<input type="text" class="form-control" id="exampleInputEmail1" required={true}/>*/}
                                    <form className={classes.container} noValidate autoComplete="off">
                                        <h5><label class="bmd-label-floating" ><strong>Name your Goal</strong></label></h5>
                                        <TextField

                                            id="standard-full-width"

                                            style={{ margin: 8 }}
                                            placeholder="Goal Name"
                                            helperText=""
                                            fullWidth
                                            margin="normal"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </form>
                                    <div><br/></div>
                                    <span class="bmd-help"></span>
                                    Goal Name Suggestion : Recommendation OR Assesment   ?  <br/> <br/>
                                    What is Goal : Goals is your Learning Analytics Objective
                                    <br/>    <br/>
                                    <MDBBtn variant="outlined" color="indigo" onClick={this.handleClickOpen}>
                                        SAVE
                                    </MDBBtn>
                                </div>
                            </form>
                        </Typography>
                    </ExpansionPanelDetails>
                    <Divider />
                    <ExpansionPanelActions>

                        <div>

                            <Dialog
                                open={this.state.open}
                                onClose={this.handleClose}
                                PaperComponent={PaperComponent}
                                aria-labelledby="draggable-dialog-title"
                            >
                                <DialogTitle id="draggable-dialog-title">REQUEST FOR NEW GOAL</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        Thank you for Input , Request will send to admin , New goal will be added shortly.
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <MDBBtn onClick={this.handleClose} color="indigo">
                                        Cancel
                                    </MDBBtn>

                                    <MDBBtn onClick={this.handleClose} color="indigo">
                                        Sent Request
                                    </MDBBtn>
                                </DialogActions>
                            </Dialog>
                        </div>
                    </ExpansionPanelActions>
                </ExpansionPanel>



            </div>
        );
    }
}

ControlledExpansionPanels.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ControlledExpansionPanels);