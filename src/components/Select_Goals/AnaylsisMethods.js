/**
 * Created by User on 10.04.2019.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';


const styles = theme => ({
    root: {
        width: '100%',
        minWidth:120,

        backgroundColor: theme.palette.background.paper,
    },
});

class AnalysisMethod extends React.Component {
    state = {
        checked: [1],
    };

    handleToggle = value => () => {
        const { checked } = this.state;
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        this.setState({
            checked: newChecked,
        });
    };
    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    render() {
        const { classes } = this.props;
        const { expanded } = this.state;

        return (
            <div className="{root}">
                <div>
                    <h6><strong><label class="bmd-label-floating">Analysis Method</label></strong>
                    </h6>
                </div>
                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Analysis Methods in OpenLAP</Typography>
                        {/*<Typography className={classes.secondaryHeading}>Expand Analysis Methods and Select</Typography>*/}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            <List dense className={classes.root}>
                                {[0, 1, 2, 3].map(value => (
                                    <ListItem key={value} button>

                                        <ListItemText primary={`Line item ${value + 1}`} />
                                        <ListItemSecondaryAction>
                                            <Checkbox
                                                onChange={this.handleToggle(value)}
                                                checked={this.state.checked.indexOf(value) !== -1}
                                            />
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                ))}
                            </List>
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>



            </div>
        );
    }
}

AnalysisMethod.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AnalysisMethod);

