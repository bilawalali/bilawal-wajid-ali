import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import {withStyles} from '@material-ui/core/styles';
import {MDBSelect, MDBSelectInput, MDBSelectOptions, MDBSelectOption} from "mdbreact";
import {MDBBtn} from 'mdbreact';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FilledInput from '@material-ui/core/FilledInput';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import ReactDOM from 'react-dom';
import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";
import OutlinedInput from '@material-ui/core/OutlinedInput';
import NoSsr from '@material-ui/core/NoSsr';
import Input from '@material-ui/core/Input';
import { indigo } from '@material-ui/core/colors';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({

    appBar: {
        position: 'relative',
    },
    root: {

        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        flexWrap: 'wrap',

    },


    noLabel: {
        marginTop: theme.spacing.unit * 3,
    },

    icon: {
        marginRight: theme.spacing.unit * 2,
    },


    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },

    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 800,

    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },

});

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};



class SimpleTabs extends React.Component {
    state = {


    };

    handleChange = (event, value,value_tab) => {
        this.setState({ value });
        this.setState({value_tab});
    };


    handleChange_tab = (event, value_tab) => {
        this.setState({value_tab});

    };

    deleteTodo() {
        this.setState({
            goal: '',
            question: '',
            organization: '',
            store: '',
            platform: '',
            activityProvider: '',
            activityProviderSource: '',
            verb: '',
            value:0
        })
    };


    render() {
        const { classes } = this.props;
        const { value } = this.state;
        const {value_tab} =this.state;

        return (

            <React.Fragment>
                <AppBar position="static" className={classes.appBar}>
                    <Toolbar>
                        <Typography variant="h6" color="inherit" noWrap>
                            OpenLAP Indicator Engine
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div className={classes.heroUnit}>
                    <div className={classes.heroContent}>
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Indicator Engine
                        </Typography>
                        <Typography variant="h6" align="center" color="textSecondary" paragraph>
                            Select Goal your LA objective , Create Question and then define indicatos for the end result
                        </Typography>

                    </div>
                </div>
                <div className={classes.layout}>
                    <Grid>
                        <hr/>
                        <form className={classes.root} autoComplete="off">
                            <FormControl variant="outlined" className={classes.formControl}>
                                <div>
                                    <h5><strong><label class="bmd-label-floating">Select Goal</label></strong></h5>
                                </div>
                                <Select
                                    value={this.state.goal}
                                    onChange={(event) => this.handleChange(event)}
                                    input={
                                        <OutlinedInput
                                            name="goal"
                                            id="outlined-goal-simple"
                                        />
                                    }
                                    name="goal"
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value="reflection">Reflection</MenuItem>
                                    <MenuItem value="recommendation">Recommendation</MenuItem>
                                    <MenuItem value="monitoring">Monitoring</MenuItem>
                                    <MenuItem value="feedback">Feedback</MenuItem>
                                    <MenuItem value="adaption">Adaption</MenuItem>
                                </Select>
                                <div><br/></div>
                                <h5><strong><label class="bmd-label-floating">Enter Your Question</label></strong></h5>
                                <input type="text" class="form-control" name="question" value={this.state.question} id="exampleInputEmail1"
                                       onChange={(event) => this.handleChange(event)}
                                />
                                <div><br/></div>

                                <strong> Example Question 1 : Which Course is best for Big Data ? </strong>
                                <div><br/></div>
                                <strong> Example Question 2 : Which Tool is best for Image Processing ?</strong>
                                <div><br/></div>
                            </FormControl>

                        </form>
                        <MDBBtn color="indigo" onClick={() => this.deleteTodo()}> New Question</MDBBtn>
                        <MDBBtn color="indigo" type="submit">Save Question</MDBBtn>
                        <hr/>
                <div className={classes.root}>
                    <AppBar position="static">
                        <Tabs value={value} onChange={this.handleChange}>
                            <Tab label="Item One" />
                            <Tab label="Item Two" />

                        </Tabs>
                    </AppBar>
                    {value === 0 && <TabContainer>

                        <div className={classes.root_tab}>
                            <AppBar position="static">
                                <Tabs value_tab={value_tab} onChange={this.handleChange_tab}>
                                    <Tab label="nested Tab1" />
                                    <Tab label="nested Tab2" />

                                </Tabs>

                            </AppBar>
                            {value_tab === 0 && <TabContainer>write code here</TabContainer>}
                            {value_tab === 1 && <TabContainer>write code here 2</TabContainer>}

                        </div>

                    </TabContainer>}
                    {value === 1 && <TabContainer>I am main Tab 2</TabContainer>}
                </div>
                    </Grid>
                </div>



            </React.Fragment>

        );
    }
}

SimpleTabs.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTabs);
