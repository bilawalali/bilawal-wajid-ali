/**
 * Created by User on 24.04.2019.
 */

/**
 * Created by User on 19.04.2019.
 */
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Toolbar from '@material-ui/core/Toolbar';
import {MDBBtn} from 'mdbreact';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FilledInput from '@material-ui/core/FilledInput';

import Select from '@material-ui/core/Select';
import ReactDOM from 'react-dom';
import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";
import Filters from './Filters'
import TextField from '@material-ui/core/TextField';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import ListBox from "./ListBox";
import Multi_Select from "./Multi_select";
import MultiSelect_Platform from "./MultiSelect_Platform";
import NestedTab from "./NestedTab";
import Mapping from "./Mapping/mapping";

const styles = theme => ({

    root: {

        paddingTop: theme.spacing.unit * 0,
        paddingBottom: theme.spacing.unit * 2,
        width:'100%',
        flexGrow: 1,
    },

    root_tab: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },

    formControl: {
        margin: theme.spacing.unit,

        width:'90%',
    },

});


function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};


function LinkTab(props) {
    return <Tab component="a" onClick={event => event.preventDefault()} {...props} />;
}

class PaperSheet extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            goal: '',
            question: '',
            organization: '',
            store: '',
            platform: '',
            activityProvider: '',
            activityProviderSource: '',
            verb: '',
            value: '',


        };
    };

    componentDidUpdate = (prevState) => {
        this.scroll()
    }

    handleChange1 = (event, value) => {
        console.log("test");
        console.log(window);
        this.setState({value});
    };

    scroll = () => {
        console.log("bilawaltest");
        window.scroll(0,0);
        window.scroll(0,1200);
    }


    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});

    };

    render () {

        const { classes } = this.props;
        const {value} = this.state;
        return (



            <div>
                <Paper className={classes.root} elevation={1}>

                    <AppBar position="static" className={classes.appBar}>
                        <Toolbar >
                            <Typography variant="h6" component="h6" color="inherit" noWrap>
                                Data Mapping
                            </Typography>
                        </Toolbar>
                    </AppBar>

                    <br/>
                    <Typography>




                        <br/> <br/>

                    </Typography>
                    <br/> <br/>


                </Paper>

            </div>

        );
    }

}







PaperSheet.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PaperSheet);