import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import {withStyles} from '@material-ui/core/styles';
import {MDBSelect, MDBSelectInput, MDBSelectOptions, MDBSelectOption} from "mdbreact";
import {MDBBtn} from 'mdbreact';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FilledInput from '@material-ui/core/FilledInput';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import ReactDOM from 'react-dom';
import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";
import OutlinedInput from '@material-ui/core/OutlinedInput';

import Input from '@material-ui/core/Input';

import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import MultiSelect_Platform from './Select_Goals/MultiSelect_Platform';
import NestedTab from './Select_Goals/NestedTab';
import Example from './Select_Goals/multi_select1';
import Example1 from './Select_Goals/Multi_select';
import  Example2 from './Select_Goals/Multi_Select_Filter';
import AnalysisMethod from './Select_Goals/AnaylsisMethods';
import ListBoxExample from './Select_Goals/ListBox';
import ActivityProvider from './Select_Goals/ActivityProvider'
import CreateGoal from './Select_Goals/CreateGoal';
import TextField from '@material-ui/core/TextField';
import NoSsr from '@material-ui/core/NoSsr';
const styles = theme => ({

    appBar: {
        position: 'fixed',

        minWidth:120,
    },
    root: {

        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,

    },

    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        Width:200,
    },
    noLabel: {
        marginTop: theme.spacing.unit * 3,
    },

    icon: {
        marginRight: theme.spacing.unit * 2,
    },


    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },

    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    formControl: {
        margin: theme.spacing.unit,
        width:'100%',
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },

});


function TabContainer(props) {
    return (
        <Typography component="div" style={{padding: 8 * 3}}>
            {props.children}
        </Typography>
    );
}


TabContainer.propTypes =
    {
        children: PropTypes.node.isRequired,
    };


class NavTabs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            goal: '',
            question: '',
            organization: '',
            store: '',
            platform: '',
            activityProvider: '',
            activityProviderSource: '',
            verb: '',
            value: '',


        };
    };


    handleChange1 = (event, value) => {

        this.setState({value});

    };

    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});

    };


    render() {
        const {classes} = this.props;
        const {value} = this.state;


        return (
            <React.Fragment>
                <AppBar position="static" className={classes.appBar}>
                    <Toolbar>
                        <Typography variant="h6" color="inherit" noWrap>
                            OpenLAP Indicator Engine
                        </Typography>
                    </Toolbar>
                </AppBar>

                <div className={classes.heroUnit}>
                    <div className={classes.heroContent}>
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Indicator Engine
                        </Typography>
                        <Typography variant="h6" align="center" color="textSecondary" paragraph>
                            Select Goal your LA objective , Create Question and then define indicators for the end result
                        </Typography>

                    </div>
                </div>
                <div className={classes.layout}>

                    <Grid>
                        <hr/>

                        <h5><strong><label class="bmd-label-floating">Goal & Question Section:</label></strong></h5>
                        <form className={classes.root} autoComplete="off">
                            <FormControl variant="outlined" className={classes.formControl}>
                                <InputLabel htmlFor="age-simple"></InputLabel>
                                <div>
                                    <h6><strong><label class="bmd-label-floating">Goal Selection:</label></strong></h6>
                                </div>
                                <Select
                                    value={this.state.goal}
                                    onChange={(event) => this.handleChange(event)}
                                    inputProps={{
                                        name: 'goal',
                                        id: 'outlined-goal-simple',
                                    }}
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value="reflection">Reflection</MenuItem>
                                    <MenuItem value="recommendation">Recommendation</MenuItem>
                                    <MenuItem value="monitoring">Monitoring</MenuItem>
                                    <MenuItem value="feedback">Feedback</MenuItem>
                                    <MenuItem value="adaption">Adaption</MenuItem>
                                </Select>
                            </FormControl>
                        </form>
                        <div><br/></div>

                        <form className={classes.root} autoComplete="off">
                            <FormControl variant="outlined" className={classes.formControl}>

                                <CreateGoal/>
                            </FormControl>
                        </form>
                        <div><br/></div>

                        <form className={classes.root} autoComplete="off">
                            <FormControl variant="outlined" className={classes.formControl}>

                                <h6><strong><label class="bmd-label-floating">Enter Your Question</label></strong></h6>
                                <TextField

                                    id="standard-full-width"

                                    style={{ margin: 8 }}
                                    placeholder="Question Name"
                                    helperText=""
                                    fullWidth
                                    margin="normal"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                <div><br/></div>
                                <strong> Example Question 1 : Which Course is best for Big Data ? </strong>
                                <div><br/></div>
                                <strong> Example Question 2 : Which Tool is best for Image Processing ?</strong>
                                <div><br/></div>
                            </FormControl>
                            <MDBBtn color="indigo"> New Question</MDBBtn>
                            <MDBBtn color="indigo" type="submit">Save Question</MDBBtn>
                        </form>

                        <hr/>
                        <h5><strong><label class="bmd-label-floating">Indicator Selection:</label></strong></h5>
                        <NoSsr>
                            <div className={classes.root}>

                                <AppBar position={'static'} >
                                    <Tabs  value={value}    onChange={this.handleChange1}>
                                        <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="Basic Indicator"/>
                                        <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="Load Existing Indicator"/>
                                        <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="Composite Indicator"/>
                                        <Tab tabItemContainerStyle={{position: "fixed", bottom:"0"}} label="MLA Indicator"/>
                                    </Tabs>
                                </AppBar>
                                {value === 0 && <TabContainer>
                                    <h5><strong><label class="bmd-label-floating">Data Field(s):</label></strong></h5>
                                    <form className={classes.root} autoComplete="off">
                                        <FormControl variant="outlined" className={classes.formControl}>

                                            <InputLabel htmlFor="age-simple"></InputLabel>
                                            <div>
                                                <h6><strong><label class="bmd-label-floating">Select Organization</label></strong></h6>
                                            </div>
                                            <Select
                                                value={this.state.organization}
                                                onChange={(event) => this.handleChange(event)}

                                                fullWidth
                                                margin="normal"
                                                inputProps={{
                                                    name:'organization',
                                                    id:'outlined-organization-simple',
                                                }}
                                            >
                                                <MenuItem value="">
                                                    <em>None</em>
                                                </MenuItem>
                                                <MenuItem value="UDE">UDE</MenuItem>
                                                <MenuItem value="RWTH Aachen">RWTH Aachen</MenuItem>
                                                <MenuItem value="HTC LAB">HTD LABS</MenuItem>
                                            </Select>
                                        </FormControl>
                                        <div><br/></div>

                                        <FormControl variant="outlined" className={classes.formControl}>
                                            <InputLabel htmlFor="age-simple"></InputLabel>
                                            <div>
                                                <h6><strong><label class="bmd-label-floating">Select Store:</label></strong></h6>
                                            </div>
                                            <Select

                                                value={this.state.store}
                                                onChange={(event) => this.handleChange(event)}
                                                fullWidth
                                                margin="normal"
                                                inputProps={{
                                                    name:'store',
                                                    id:'outlined-store-simple',
                                                }}
                                            >
                                                name="store"
                                                >
                                                <MenuItem value="">
                                                    <em>None</em>
                                                </MenuItem>
                                                <MenuItem value="{Social Computing}">Social Computing</MenuItem>
                                                <MenuItem value="{Information Systems}">Information Systems</MenuItem>
                                                <MenuItem value="{Distributed Systems}">Distributed Systems</MenuItem>
                                            </Select>



                                        </FormControl>
                                        <div><br/></div>

                                        {/*platform multiselect , wo saare component jo alag banaye hai unko formcontrol ke baher call kerna hai */}
                                        <div>
                                            <MultiSelect_Platform  />
                                        </div>
                                        <div><br/></div>

                                        <div>
                                            <ActivityProvider />
                                        </div>


                                        <div>
                                            <Example/>
                                        </div>
                                        <div><br/></div>

                                        <div>
                                            <Example1/>
                                        </div>


                                        <div><br/></div>

                                        <div>
                                            <div>
                                                <h5><strong><label class="bmd-label-floating">Filter Value for Attributes:</label></strong></h5>
                                            </div>
                                            <NestedTab/>
                                        </div>


                                    </form>



                                </TabContainer>}

                                {value === 1 && <TabContainer>Page Two</TabContainer>}
                                {value === 2 && <TabContainer>Page Three</TabContainer>}
                                {value === 3 && <TabContainer>Page Four</TabContainer>}

                            </div>
                        </NoSsr>


                    </Grid>
                </div>

            </React.Fragment>

        );
    }
}


NavTabs.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavTabs);