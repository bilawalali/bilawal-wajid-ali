import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import {indigo} from '@material-ui/core/colors';

function TabContainer(props) {
    return (
        <Typography component="div" style={{padding: 8 * 3}}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: indigo
    },

});

class SimpleTabs extends React.Component {
    state = {};

    handleChange = (event, value, value_tab) => {
        this.setState({value});
        this.setState({value_tab});
    };

    handleChange_tab = (event, value_tab) => {
        this.setState({value_tab});

    };

    render() {
        const {classes} = this.props;
        const {value} = this.state;
        const {value_tab} = this.state;

        return (

            <React.Fragment>

                <div className={classes.root}>
                    <AppBar position="static">
                        <Tabs value={value} onChange={this.handleChange}>
                            <Tab label="Item One"/>
                            <Tab label="Item Two"/>

                        </Tabs>
                    </AppBar>
                    {value === 0 && <TabContainer>

                        <div className={classes.root_tab}>
                            <AppBar position="static">
                                <Tabs value_tab={value_tab} onChange={this.handleChange_tab}>
                                    <Tab label="nested Tab1"/>
                                    <Tab label="nested Tab2"/>

                                </Tabs>

                            </AppBar>
                            {value_tab === 0 && <TabContainer>write code here</TabContainer>}
                            {value_tab === 1 && <TabContainer>write code here 2</TabContainer>}

                        </div>

                    </TabContainer>}
                    {value === 1 && <TabContainer>I am main Tab 2</TabContainer>}

                </div>


            </React.Fragment>

        );
    }
}

SimpleTabs.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTabs);