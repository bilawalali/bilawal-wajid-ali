import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';
import { MDBSelect, MDBSelectInput, MDBSelectOptions, MDBSelectOption} from "mdbreact";
import { MDBBtn} from 'mdbreact';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";




const styles = theme => ({

    appBar: {
        position: 'relative',
    },
    root: {
        flexGrow: 4,
        backgroundColor:   theme.palette.background.paper,
        display: 'flex',
        flexWrap: 'wrap',
    },


    noLabel: {
        marginTop: theme.spacing.unit * 3,
    },

    icon: {
        marginRight: theme.spacing.unit * 2,
    },


    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },

    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
    root1: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    footer:

        {
            height: '10px',
            marginTop: '-50px'
        }

});

class Upload_Vmodules extends React.Component
{

    render() {
        const { classes } = this.props;


        return (
            <React.Fragment>
                <CssBaseline />
                <AppBar position="static" className={classes.appBar}>
                    <AppBar position="static">
                        <div className={classes.root1}>
                            <Toolbar>
                                <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                                    <MenuIcon />
                                </IconButton>
                                <Typography variant="h6" color="inherit" className={classes.grow}>
                                    Open Learning Analytics Platform (OpenLAP)

                                </Typography>

                                <Button color="inherit" >Logout</Button>



                            </Toolbar>
                        </div>
                    </AppBar>
                </AppBar>
                <main>
                    {/* Hero unit */}
                    <div className={classes.heroUnit}>
                        <div className={classes.heroContent}>
                            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                                Visualization  Modules
                            </Typography>
                            <br/> <br/>
                            <Typography variant="h6" align="center" color="textSecondary" paragraph>
                                1. Upload your Visualization Module  <br/>
                                2. Your Visualization Module upload request routed to the Admin. <br/>
                                3. Admin will review the request and will approve that request. <br/>
                                4. Once Admin approved the request, Visualization Module will reflect in OpenLAP Indicator Engine<br/>
                            </Typography>

                        </div>
                    </div>
                    <div className={(classes.layout)}>
                        {/* End hero unit */}
                        <br/>
                        <Grid >

                            <hr/>

                            <div class="input-default-wrapper mt-3">

                                <span class="input-group-text mb-3" id="input1">Upload</span>

                                <input type="file" id="file-with-current" class="input-default-js" />

                                <label class="label-for-default-js rounded-right mb-3" for="file-with-current"><span class="span-choose-file">Choose file</span>

                                    <div class="float-right span-browse">Browse</div>

                                </label>

                            </div>
                            What is Visualization Modules?  <br/> <br/>
                            Visualization Modules , enables you to select different visualization modules as per your need to visualize your results
                            <br/>    <br/>
                            <MDBBtn color="primary" type="submit" >Submit File</MDBBtn>
                        </Grid>


                    </div>
                    <br/>
                </main>
                <br/><br/>   <br/><br/>  <br/><br/>  <br/><br/> <br/>
                {/* Footer */}
                <div className={classes.footer}>
                    <br/><br/><br/>
                    <footer class="page-footer text-center font-small mdb-color darken-2 "  >

                        <hr/>

                        <div className="pb-4" style={{height: "4rem" }}>

                            <h6 style={{}}>Follow us on Git Hub </h6>
                            <a href="https://github.com/mdbootstrap/bootstrap-material-design" target="_blank">
                                <i class="fab fa-github mr-3"></i>
                            </a>


                        </div>


                    </footer>
                </div>



            </React.Fragment>
        );
    }
}



Upload_Vmodules.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(Upload_Vmodules);