/**
 * Created by User on 14.05.2019.
 */
import React from "react";
import { render } from "react-dom";

const footerStyle = {
    backgroundColor: "purple",
    fontSize: "20px",
    color: "white",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "60px",
    width: "100%"
};

const phantomStyle = {
    display: "block",
    padding: "20px",
    height: "60px",
    width: "100%"
};

function Footer({ children }) {
    return (
        <div>
            <div style={phantomStyle} />
            <div style={footerStyle}>{children}</div>
        </div>
    );
}

function Hello() {
    return (
        <div>
            <div>Top Content!</div>

            <div>Sandwich: Keep Scrolling!</div>

            <div>More Sandwich: Yum!</div>

            <div>You can scroll and see me!</div>
        </div>
    );
}

render(
    <div>
        <Hello />
        <Footer>
            <span>footer content</span>
        </Footer>
    </div>,
    document.getElementById("root")
);

