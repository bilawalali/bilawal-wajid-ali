import React from 'react';
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import {withStyles} from '@material-ui/core/styles';

import Toolbar from '@material-ui/core/Toolbar'

import Typography from '@material-ui/core/Typography';

import {MDBSelect, MDBSelectInput, MDBSelectOptions, MDBSelectOption} from "mdbreact";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {
    // State or Local Processing Plugins
} from '@devexpress/dx-react-grid';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";
import MyGridTable from './View_Eindicators/Table';
import Button from '@material-ui/core/Button';
const styles = theme => ({

    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },

    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },

    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    root1:{
        flexGrow: 1,
    },
    footer:

        {
            height: '10px',
            marginTop: '-50px'
        }

});

class View_Eindicators extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {classes} = this.props;
        return (
            <React.Fragment>
                <CssBaseline />
                <AppBar position="static" className={classes.appBar}>
                    <AppBar position="static">
                        <div className={classes.root1}>
                            <Toolbar>
                                <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                                    <MenuIcon />
                                </IconButton>
                                <Typography variant="h6" color="inherit" className={classes.grow}>
                                    Open Learning Analytics Platform (OpenLAP)

                                </Typography>

                                <Button color="inherit" >Logout</Button>



                            </Toolbar>
                        </div>
                    </AppBar>
                </AppBar>
                <main>
                    <div className={classes.heroUnit}>
                        <div className={classes.heroContent}>
                            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                                Existing Indicators
                            </Typography>
                            <Typography variant="h6" align="center" color="textSecondary" paragraph>
                                Here you can enable to see the all exisitng indicators which are created previously
                                <br/>
                                So you have an idea how the indicators looks-a-like.
                            </Typography>

                        </div>
                    </div>
                    {/*as a component called*/}
                    <MyGridTable />
                </main>
                <div className={classes.footer}>
                    <br/><br/><br/>
                    <footer class="page-footer text-center font-small mdb-color darken-2 "  >

                        <hr/>

                        <div className="pb-4" style={{height: "4rem" }}>

                            <h6 style={{}}>Follow us on Git Hub </h6>
                            <a href="https://github.com/mdbootstrap/bootstrap-material-design" target="_blank">
                                <i class="fab fa-github mr-3"></i>
                            </a>


                        </div>


                    </footer>
                </div>
            </React.Fragment>

        );

    }
}


View_Eindicators.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(View_Eindicators);

