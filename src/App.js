import React, { Component } from 'react';

import {BrowserRouter, Route, Switch, Link} from 'react-router-dom';

import View_Eindicators from './components/View_Eindicators';
import FAQ from './components/FAQ';
import Create_Goal from './components/Create_Goal';
import Upload_Amodules from './components/Upload_Amodules';
import Upload_Vmodules from './components/Upload_Vmodules';
import Select_Goals from "./components/Select_Goals";




import SimpleSelect from "./components/SimpleSelect";

import "@kenshooui/react-multi-select/dist/style.css"



import Home from './components/Home';
import SignIn from "./components/SignIn";
import SignUp from './components/SignUp';
import AdminHome from './components/AdminHome';
class App extends Component {


    render() {
        return (
            <div className="App">


                <BrowserRouter>
                    <Switch>
                        <Route path="/" component={Home} exact />
                        <Route path="/View_Eindicators" component={View_Eindicators}/>

                        <Route path="/Upload_Vmodules" component={Upload_Vmodules}/>
                        <Route path="/Upload_Amodules" component={Upload_Amodules}/>
                        <Route path="/Create_Goal" component={Create_Goal}/>
                        <Route path="/FAQ" component={FAQ}/>
                        <Route path="/Select_Goals" component={Select_Goals}/>
                        <Route path ="/SignIn" component={SignIn}/>
                        <Route path ="/SignUp" component={SignUp}/>
                        <Route path ="/AdminHome" component={AdminHome}/>

                    </Switch>


                </BrowserRouter>
            </div>
        );
    }
}

export default App;