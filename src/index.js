import React from 'react';
import {render} from 'react-dom';
import "mdbreact/dist/css/mdb.css";
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import App from './App';


const theme=createMuiTheme(
    {

    palette: {
       primary: {
            main: '#01579b',
        },

    },


})
console.log(theme);
render(
    <MuiThemeProvider theme={theme}>
    <App />,
    </MuiThemeProvider>,
    document.getElementById('root')
);

